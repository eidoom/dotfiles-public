#!/usr/bin/env bash

./dotfiles-zsh/install.sh
startSuper=$(pwd)
hereSuper=$(realpath "$0" | xargs dirname)
cd "$hereSuper" || exit
submodules=$(git submodule foreach --quiet "git rev-parse --show-toplevel")
for submodule in $submodules; do
    if [[ "$submodule" != "$hereSuper/dotfiles-zsh" ]]; then
        "$submodule"/install.sh
    fi
done
cd "$startSuper" || exit
