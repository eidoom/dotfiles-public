# dotfiles-public

## Description

Installation scripts and configurations for my Linux environment.
System package installations are specific to the [`dnf`](https://fedoraproject.org/wiki/DNF) package manager on the distro [Fedora](https://getfedora.org/) and tested on Fedora 31.

Scripts and settings for programs are organised into submodules.

## Usage

-   Install with 

    -   SSH clone (if you're me)

    ```shell
    git clone --recurse-submodules -j8 git@gitlab.com:eidoom/dotfiles-public.git .dotfiles-public
    ```

    -   or HTTPS clone (if you're not me)

    ```shell
    git clone --depth 1 --recurse-submodules -j8 https://gitlab.com/eidoom/dotfiles-public.git .dotfiles-public
    ```

-   then run installation script

```shell
cd .dotfiles-public
./install.sh
```

-   log out and in again to effect the change of shell to `zsh`
-   in `tmux`, `prefix`+`I` to install plugins
-   `nvim` will install plugins automatically on first startup

## Troubleshooting

* If attempts to clone return `error: unknown switch 'j'`, you need to update your `git` client. Alternatively, run the command without `-j8`, which is only there to parallelise submodule downloads.
* If there are any problems, just run the installation script a second time.

## Updating

```shell
git pull
git submodule update --init --recursive --remote --jobs=8
./install.sh
```

# Related repos
Links to my `dotfiles-private` repos (may require permissions to access)

* [dotfiles-home-server](https://gitlab.com/eidoom/dotfiles-home-server) for my home server (Debian 10)
* [dotfiles-work-laptop](https://gitlab.com/eidoom/dotfiles-work-laptop) for the Lenovo ThinkPad X280 (Fedora 31)
* [dotfiles-work-laptop-xps](https://gitlab.com/eidoom/dotfiles-work-laptop-xps) for the Dell XPS 15 9560 (Fedora 31)
* [dotfiles-home-desktop](https://gitlab.com/eidoom/dotfiles-home-desktop) for my home desktop
